import os
import re
import sys
import math
import shutil
import argparse
import subprocess
from progress.bar import Bar

def run_test(executable, exec_path, nrThreads, output_path):
    if executable:
        subprocess.call([exec_path, "1", "500000", str(nrThreads)], stdout=output_path)
    else: # basicly java
        pass

def main(argv):
    parser = argparse.ArgumentParser(description="Running superperfect numbers implementation to find the run time.")

    parser.add_argument("--C", action='store_true', help="Run a C implementation of superperfect numbers.")
    parser.add_argument("--JAVA", action='store_true', help="Run a JAVA implementation of superperfect numbers.")

    parser.add_argument("-s", "--serial_path", action='store', dest='serial_path', help="Defines the path to the serial implementation.")
    parser.add_argument("-p", "--paralel_path", action='store', dest='paralel_path', help="Defines the path to the paralel implementation.")

    parser.add_argument("-n", "--number_of_runs", action='store', dest='number_of_runs', type=int, help="Defines the number of times the implementation has to be run.")
    parser.add_argument("-j", "--number_of_threads", action='store', dest='number_of_threads', type=int, help="Defines the number of threads to run the paralel implementation.")

    parsed = parser.parse_args(argv[1:])

    # print("C: " + str(parsed.C))
    # print("JAVA: " + str(parsed.JAVA))
    # print("serial_path: " + str(parsed.serial_path))
    # print("paralel_path: " + str(parsed.paralel_path))
    # print("number_of_runs: " + str(parsed.number_of_runs))
    # print("number_of_threads: " + str(parsed.number_of_threads))

    if parsed.number_of_runs <= 0:
        print("The number of runs has to be greater than 0.")
        exit(0)
    
    if parsed.number_of_threads <= 0:
        print("The number of threads has to be greater than 0.")
        exit(0)

    time_regex = re.compile(r'(\d+)\sms\s(\d+)\sus')

    max_progress = parsed.number_of_runs + parsed.number_of_threads * parsed.number_of_runs
    bar = Bar('Processing', max=max_progress, suffix='%(index)d/%(max)d - %(percent).1f%% - %(eta)ds')

    with open("results.csv", "w") as results:
        results.write("test_name,time_in_microseconds\n")

        if os.path.exists("serial"):
            shutil.rmtree("serial")

        os.mkdir("serial")
        total_sum = 0

        for i in range(parsed.number_of_runs):
            with open(os.path.join("serial", str(i) + ".txt"), "w") as fout:
                run_test(parsed.C, parsed.serial_path, 1, fout)
            with open(os.path.join("serial", str(i) + ".txt"), "r") as fin:
                line = fin.readline()
                line = fin.readline()

                match = time_regex.match(line)

                if match is not None:
                    groups = match.groups()
                    # print(groups)
                    total_sum += int(groups[0]) * 1000 + int(groups[1])
            bar.next()
        
        results.write("serial," + str(math.ceil(total_sum / parsed.number_of_runs)) + "\n")

        if os.path.exists("paralel"):
            shutil.rmtree("paralel")

        os.mkdir("paralel")

        for nrThreads in range(parsed.number_of_threads):
            os.mkdir(os.path.join("paralel", str(nrThreads + 1)))
            total_sum = 0

            dir_path = os.path.join("paralel", str(nrThreads + 1))

            for i in range(parsed.number_of_runs):
                with open(os.path.join(dir_path, str(i) + ".txt"), "w") as fout:
                    run_test(parsed.C, parsed.paralel_path, nrThreads + 1, fout)
                with open(os.path.join(dir_path, str(i) + ".txt"), "r") as fin:
                    line = fin.readline()
                    line = fin.readline()

                    match = time_regex.match(line)

                    if match is not None:
                        groups = match.groups()
                        # print(groups)
                        total_sum += int(groups[0]) * 1000 + int(groups[1])
                bar.next()
            
            results.write("paralel_" + str(nrThreads + 1) + "," + str(math.ceil(total_sum / parsed.number_of_runs)) + "\n")
    print("")

if __name__ == '__main__':
    main(sys.argv)