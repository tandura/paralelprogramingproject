#include "Threads.h"

void createThread(PTHREAD_IDENTIFIER thread_id, void (*func)(), void *date)
{
#if defined(DD_WINDOWS)
	thread_id->threadId = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)func, date, NULL, NULL);
#elif defined(DD_LINUX)
	pthread_create(&thread_id->threadId, NULL, (void* (*)(void*))func, date);
#endif
}


void joinThreads(THREAD_IDENTIFIER thread_id)
{
#if defined(DD_WINDOWS)
	WaitForSingleObject(thread_id.threadId, INFINITE);
#elif defined(DD_LINUX)
	pthread_join(thread_id.threadId, NULL);
#endif
}