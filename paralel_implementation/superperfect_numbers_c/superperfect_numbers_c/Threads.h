#ifndef __THREADS_H__
#define __THREADS_H__

#include "Defines.h"

#if defined(DD_WINDOWS)
#include <Windows.h>
#elif defined(DD_LINUX)
#include <pthread.h>
#endif


typedef struct
{
#if defined(DD_WINDOWS)
	HANDLE  threadId;
#elif defined(DD_LINUX)
	pthread_t threadId;
#endif
}THREAD_IDENTIFIER, *PTHREAD_IDENTIFIER;

void createThread(PTHREAD_IDENTIFIER thread_id, void(*func)(), void *date);
void joinThreads(THREAD_IDENTIFIER thread_id);

#endif