#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "Defines.h"
#include "Threads.h"
#include "MeasureTime.h"

typedef struct
{
	UInt64 start;
	UInt64 end;
	UInt64 res[100];
	UInt32 index;
	DD_TIME_INTERVAL time_interval;
}THREAD_DATA, *PTHREAD_DATA;

#define BILLION  1000000000L;
#define MAX_THREADS 128

// The results will be store here
// Is used to isolate the computation from display
THREAD_DATA results[MAX_THREADS];
THREAD_IDENTIFIER t_ids[MAX_THREADS];


UInt64 divisor_sum(UInt64 n)
{
	UInt64 divisor = 0;

	// the number itself is a divisor
	UInt64 sum = 0; 

	// The search upper limit is the n / 2
	// There will be no more divisors past this limit
	UInt64 limit = (UInt64)sqrtl((long double)n);

	for (UInt64 i = 1; i <= limit; i++)
	{
		if(n % i == 0)
		{
			sum += i;

			// For every divisor i of N, 
			// there is also a corresponding divisisor N/i
			divisor = n / i;
			
			if (divisor != i) 
			{
				sum += divisor;
			}
		}
	}

	return sum;
}

void testInterval(UInt64 A, UInt64 B, UInt64 *res, UInt32 *res_index)
{
	UInt64 sum = 0;
	*res_index = 0;

	for (UInt64 i = A; i < B; i++)
	{
		sum = divisor_sum(i);
		sum = divisor_sum(sum);

		if (sum == 2 * i) {
			res[*res_index] = i;
			(*res_index)++;
		}
	}

	
}

#if defined(DD_WINDOWS)
DWORD WINAPI threadFunction(LPVOID lpParam)
{
	PTHREAD_DATA pThread_data = (PTHREAD_DATA)lpParam;
	//printf("start: %lld  end: %lld\n", pThread_data->start, pThread_data->end);
	//read_time(&pThread_data->time_interval.start);
	testInterval(pThread_data->start, pThread_data->end, pThread_data->res, &pThread_data->index);
	//read_time(&pThread_data->time_interval.end);
	return 0;
}
#elif defined(DD_LINUX)
void *threadFunction(void *buff)
{
	PTHREAD_DATA pThread_data = (PTHREAD_DATA)buff;
	read_time(&pThread_data->time_interval.start);
	testInterval(pThread_data->start, pThread_data->end, pThread_data->res, &pThread_data->index);
	read_time(&pThread_data->time_interval.end);
	return NULL;
}
#endif


void printUsage() 
{
	printf("Usage: superperfect_numbers_c.exe A B [T]\n");
	printf("        Finds all the superperfect numbers in interva [A, B) (B is not tested),\n");
	printf("        where A and B are natural numbers.\n");
	printf("        T is the number of threads for the program to run (default: 1, max: %d).\n\n", MAX_THREADS);
	printf("Output: All the superperfect numbers in interval [A, B] separated by a space.\n");
	printf("        On the next line it will display the time neaded to compute them.\n");
}

int main(int argc, const char *argv[])
{
	UInt64 A = 0;
	UInt64 B = 0;
	UInt64 sum = 0;
	UInt32 nrThreads = 1;
	int results_index = 0;
	DD_TIME_INTERVAL time_interval;
	DD_TIME_INTERVAL join_interval;

	if (argc < 2) {
		printUsage();
		return 0;
	}

	A = atoi(argv[1]);
	B = atoi(argv[2]);

	if (argc == 4) {
		nrThreads = atoi(argv[3]);
		if (nrThreads > MAX_THREADS) {
			printUsage();
			return 0;
		}
	}

	read_time(&time_interval.start);

	for (UInt64 i = 0; i < nrThreads; i++)
	{
		results[i].start = A + (B - A) / nrThreads * i;
		results[i].end = A + (B - A) / nrThreads * (i + 1);
		createThread(&t_ids[i], (void(*)())threadFunction, &results[i]);
	}

	for (UInt64 i = 0; i < nrThreads; i++)
	{
		joinThreads(t_ids[i]);
	}

	read_time(&time_interval.end);

	join_interval.start = results[0].time_interval.end;
	join_interval.end = time_interval.end;


	for (UInt64 i = 0; i < nrThreads; i++)
	{
		for (UInt32 j = 0; j < results[i].index; j++)
		{
			printf("%lld ", results[i].res[j]);
		}
	}
	printf("\n");
	printf("%lld ms ", diff_milli(time_interval));
	printf("%lld us\n", diff_micro(time_interval) % 1000);

	/*printf("\n");
	printf("%lld ms ", diff_milli(results[0].time_interval));
	printf("%lld us\n", diff_micro(results[0].time_interval) % 1000);

	printf("\n");
	printf("%lld ms ", diff_milli(join_interval));
	printf("%lld us\n", diff_micro(join_interval) % 1000);*/

	return 0;
}