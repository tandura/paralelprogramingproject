#include "MeasureTime.h"

void read_time(void *time_buffer)
{
#if defined(DD_WINDOWS)
	QueryPerformanceCounter((LARGE_INTEGER *)time_buffer);
#elif defined(DD_LINUX)
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, (struct timespec *)time_buffer);
#endif
}

Int64 diff_micro(DD_TIME_INTERVAL time_interval)
{
#if defined(DD_WINDOWS)
	LARGE_INTEGER Frequency, elapsed;

	QueryPerformanceFrequency(&Frequency);
	elapsed.QuadPart = time_interval.end.QuadPart - time_interval.start.QuadPart;

	elapsed.QuadPart *= 1000000;
	elapsed.QuadPart /= Frequency.QuadPart;

	return elapsed.QuadPart;
#elif defined(DD_LINUX)
	return ((time_interval.end->tv_sec * (1000000)) + (time_interval.end->tv_nsec / 1000)) -
		((time_interval.start->tv_sec * 1000000) + (time_interval.start->tv_nsec / 1000));
#endif
}


Int64 diff_milli(DD_TIME_INTERVAL time_interval)
{
#if defined(DD_WINDOWS)
	LARGE_INTEGER Frequency, elapsed;

	QueryPerformanceFrequency(&Frequency);
	elapsed.QuadPart = time_interval.end.QuadPart - time_interval.start.QuadPart;

	elapsed.QuadPart *= 1000;
	elapsed.QuadPart /= Frequency.QuadPart;

	return elapsed.QuadPart;
#elif defined(DD_LINUX)
	return ((time_interval.end->tv_sec * 1000) + (time_interval.end->tv_nsec / 1000000)) -
		((time_interval.start->tv_sec * 1000) + (time_interval.start->tv_nsec / 1000000));
#endif
}