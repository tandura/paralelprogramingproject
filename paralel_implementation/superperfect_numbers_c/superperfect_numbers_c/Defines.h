#ifndef __DAN_DEFINES_H__

#ifdef _WIN32
	//define something for Windows (32-bit and 64-bit, this part is common)
	#define DD_WINDOWS

	#ifdef _WIN64
		//define something for Windows (64-bit only)
	#else
		//define something for Windows (32-bit only)
	#endif
#elif __APPLE__
	#include "TargetConditionals.h"
	#if TARGET_IPHONE_SIMULATOR
		// iOS Simulator
	#elif TARGET_OS_IPHONE
		// iOS device
	#elif TARGET_OS_MAC
		// Other kinds of Mac OS
	#else
	#   error "Unknown Apple platform"
	#endif
#elif __GNUC__
	// linux
	#define DD_LINUX
#elif __unix__ // all unices not caught above
	// Unix
#elif defined(_POSIX_VERSION)
	// POSIX
#else
	#   error "Unknown compiler"
#endif

#define Int64 long long int
#define UInt64 unsigned long long int
#define Int32 int
#define UInt32 unsigned int

#endif // !__DAN_DEFINES_H__

