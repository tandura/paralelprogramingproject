#ifndef __MEASURE_TIME_H__
#define __MEASURE_TIME_H__

#include "Defines.h"

#if defined(DD_WINDOWS)
#include <Windows.h>
#elif defined(DD_LINUX)
#include <time.h>
#endif

typedef struct
{
#if defined(DD_WINDOWS)
	LARGE_INTEGER start;
	LARGE_INTEGER end;
#elif defined(DD_LINUX)
	struct timespec start;
	struct timespec end;
#endif
}DD_TIME_INTERVAL;

void read_time(void *time_buffer);

Int64 diff_micro(DD_TIME_INTERVAL time_interval);
Int64 diff_milli(DD_TIME_INTERVAL time_interval);

#endif // 

