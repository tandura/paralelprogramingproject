#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "Defines.h"
#include "MeasureTime.h"

#define BILLION  1000000000L;

// The results will be store here
// Is used to isolate the computation from display
UInt64 results[500000];


UInt64 divisor_sum(UInt64 n)
{
	UInt64 divisor = 0;

	// the number itself is a divisor
	UInt64 sum = 0; 

	// The search upper limit is the n / 2
	// There will be no more divisors past this limit
	UInt64 limit = (UInt64)sqrtl((long double)n);

	for (UInt64 i = 1; i <= limit; i++)
	{
		if(n % i == 0)
		{
			sum += i;

			// For every divisor i of N, 
			// there is also a corresponding divisisor N/i
			divisor = n / i;
			
			if (divisor != i) 
			{
				sum += divisor;
			}
		}
	}

	return sum;
}


int main(int argc, const char *argv[])
{
	UInt64 A = 0;
	UInt64 B = 0;
	UInt64 sum = 0;
	int results_index = 0;
	DD_TIME_INTERVAL time_interval;

	if (argc < 2) {
		printf("Usage: superperfect_numbers_c.exe A B\n");
		printf("        Finds all the superperfect numbers in interva [A, B] (inclusive),\n");
		printf("        where A and B are natural numbers.\n\n");
		printf("Output: All the superperfect numbers in interval [A, B] separated by a space.\n");
		printf("        On the next line it will display the time neaded to compute them.\n");
		return 0;
	}

	A = atoi(argv[1]);
	B = atoi(argv[2]);

	read_time(&time_interval.start);

	for (UInt64 i = A; i <= B; i++)
	{
		sum = divisor_sum(i);
		sum = divisor_sum(sum);

		if (sum == 2 * i) {
			results[results_index] = i;
			results_index++;
		}
	}

	read_time(&time_interval.end);

	for (int i = 0; i < results_index; i++)
	{
		printf("%lld ", results[i]);
	}
	printf("\n");
	printf("%lld ms ", diff_milli(time_interval));
	printf("%lld us\n", diff_micro(time_interval) % 1000);

	return 0;
}