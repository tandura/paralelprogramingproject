divisor(N,P) :-
    P>0,
    N>=P,
    0 is N mod P.

divisor_sum_acc(N, 1, Psum, Sum):-
    NPsum is Psum + N,
    Sum is NPsum + 1, !.

divisor_sum_acc(N, P, Psum, Sum):-
    divisor(N, P),
    NPsum is Psum + P,
    ND is N div P,
    ND =\= P,
    NNPsum is NPsum + ND,    
    NP is P - 1,
    divisor_sum_acc(N, NP, NNPsum, Sum).

divisor_sum_acc(N, P, Psum, Sum):-
    divisor(N, P), !,
    NPsum is Psum + P,
    NP is P - 1,
    divisor_sum_acc(N, NP, NPsum, Sum).

divisor_sum_acc(N, P, Psum, Sum):-
    NP is P - 1,
    divisor_sum_acc(N, NP, Psum, Sum).

divisor_sum(N, Sum):-
    Z is sqrt(N),
    P is floor(Z),
    divisor_sum_acc(N, P, 0, Sum).

superperfect(N):-
    divisor_sum(N, Sum),
    divisor_sum(Sum, NSum), !,
    Rez is N * 2,
    Rez == NSum.


    
superperfect_numbers_aux(A, A, [A]):-
    superperfect(A), !.

superperfect_numbers_aux(A, A, []).

superperfect_numbers_aux(A, B, [A|T]):-
    superperfect(A), !,
    NA is A + 1,
    superperfect_numbers_aux(NA, B, T).

superperfect_numbers_aux(A, B, T):-
    NA is A + 1,
    superperfect_numbers_aux(NA, B, T).


superperfect_numbers(A, B):-
    statistics(walltime, [_ | [_]]),
    superperfect_numbers_aux(A, B, L), !,
    statistics(walltime, [_ | [ExecutionTime]]),
    write(L), nl,
    write('Execution took '), write(ExecutionTime), write(' ms.'), nl.