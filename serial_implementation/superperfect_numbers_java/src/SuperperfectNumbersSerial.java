import java.math.BigInteger;
import java.util.ArrayList;

public class SuperperfectNumbersSerial {

    private static ArrayList<BigInteger> results = new ArrayList<>(1000);

    private static BigInteger sqrt(BigInteger x) {
        BigInteger div = BigInteger.ZERO.setBit(x.bitLength()/2);
        BigInteger div2 = div;

        // Loop until we hit the same value twice in a row, or wind
        // up alternating.
        for(;;) {
            BigInteger y = div.add(x.divide(div)).shiftRight(1);
            if (y.equals(div) || y.equals(div2))
                return y;
            div2 = div;
            div = y;
        }
    }

    private static  BigInteger divisor_sum(BigInteger n)
    {
        BigInteger divisor;

        // the number itself is a divisor
        BigInteger sum = BigInteger.ZERO;

        // The search upper limit is the n / 2
        // There will be no more divisors past this limit
        BigInteger limit = sqrt(n);

        for (BigInteger i = BigInteger.ONE; i.compareTo(limit) <= 0; i = i.add(BigInteger.ONE))
        {
            if(n.remainder(i).equals(BigInteger.ZERO))
            {
                sum = sum.add(i);

                // For every divisor i of N,
                // there is also a corresponding divisisor N/i
                divisor = n.divide(i);

                if (!divisor.equals(i))
                {
                    sum = sum.add(divisor);
                }
            }
        }

        return sum;
    }

    public static void main(String[] args) {

        if(args.length < 2){
            System.out.print("Usage: superperfect_numbers_c.exe A B\n");
            System.out.print("        Finds all the superperfect numbers in interva [A, B] (inclusive),\n");
            System.out.print("        where A and B are natural numbers.\n\n");
            System.out.print("Output: All the superperfect numbers in interval [A, B] separated by a space.\n");
            System.out.print("        On the next line it will display the time neaded to compute them.\n");
            return;
        }

        BigInteger A = new BigInteger(args[0]);
        BigInteger B = new BigInteger(args[1]);

        BigInteger sum;
        Long start, end, time_interval;


        start = System.nanoTime();

        for (BigInteger i = A; i.compareTo(B) <= 0; i = i.add(BigInteger.ONE)){
            sum = divisor_sum(i);
            sum = divisor_sum(sum);

            if (sum.equals(new BigInteger("2").multiply(i))) {
                results.add(i);
            }
        }

        end = System.nanoTime();

        for(BigInteger superperfect : results){
            System.out.print(superperfect.toString() + " ");
        }
        System.out.println();

        time_interval = end - start;

        System.out.println((time_interval / 1000000) + " ms " + ((time_interval % 1000000) / 1000) + " us");

    }
}
